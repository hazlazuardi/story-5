from django.contrib import admin

from .models import Person, ClassYear
# Register your models here.

admin.site.register(Person)
admin.site.register(ClassYear)
