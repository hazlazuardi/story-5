from django.db import models

# Create your models here.



class ClassYear(models.Model):
    Year = models.CharField(primary_key=True, max_length=20)
    def __str__(self):
        return f"{self.Year}"



class Person(models.Model):
    Name = models.CharField(primary_key=True, max_length=20)
    Hobby = models.CharField(max_length=20)
    Favorite = models.CharField(max_length=20)
    Year = models.ForeignKey(ClassYear, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.Name}"
