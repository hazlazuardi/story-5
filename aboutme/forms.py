from django import forms
from .models import Person, ClassYear

# Film is Person
# Studio is ClassYear

class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = (
            'Name',
            'Hobby',
            'Favorite',
            'Year'
        )

    # only need to define `new_studio`, other fields come automatically from model
    new_year = forms.CharField(max_length=30, required=False, label = "New Class Year Name")

    def __init__(self, *args, **kwargs):
        super(PersonForm, self).__init__(*args, **kwargs)
        # make `studio` not required, we'll check for one of `studio` or `new_studio` in the `clean` method
        self.fields['Year'].required = False

    def clean(self):
        # raise AttributeError()
        year = self.cleaned_data.get('Year')
        new_year = self.cleaned_data.get('new_year')
        if not year and not new_year:
            # neither was specified so raise an error to user
            raise forms.ValidationError('Must specify either Year or New Year!')
        elif not year:
            # get/create `Studio` from `new_studio` and use it for `studio` field
            year, created = ClassYear.objects.get_or_create(Year=new_year)
            self.cleaned_data['Year'] = year

        
        return super(PersonForm, self).clean()




# class POSTPerson(forms.ModelForm):
#     class Meta:
#         model = Person
#         fields = (
#         'Name',
#         'Hobby',
#         'Favorite',
#         )

# class POSTClassYear(forms.ModelForm):
#     class Meta:
#         model = ClassYear
#         fields = (
#             'Year',
#         )


# class PersonForm(forms.Form):
#     Name = forms.CharField(label='Name', max_length=20)
#     Hobby = forms.CharField(label='Hobby', max_length=20)
#     Favorite = forms.CharField(label='Favorite', max_length=20)
#     Year = forms.CharField(label='Year', max_length=20)
