from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import messages

from .models import Person, ClassYear
from .forms import PersonForm

# Create your views here.

def index(request):
    flist = Person.objects.all()
    context = {
        'flist':flist,
    }
    return render(request, 'aboutme/index.html', context)


def addfriends(request):
    # person_form = POSTPerson()
    # year_form = POSTClassYear()
        # if this is a POST request we need to process the form data
    if request.method == 'POST':
        form = PersonForm(request.POST)
        # create a orm instance and populate it with data from the request:
        # person_form = POSTPerson(request.POST)
        # year_form = POSTClassYear(request.POST)

        if form.is_valid():
            form.save() 

        # year = POSTClassYear(request.POST)

        # check whether it's valid:
        # if person_form.is_valid() and year_form.is_valid():
            # person = Person(
            #     Name = request.POST.get('Name'),
            #     Hobby = request.POST.get('Hobby'),
            #     Favorite = request.POST.get('Favorite'),
            #     Year = ClassYear(
            #     Year = request.POST.get('Year')
            # )
            #     )
            
            # year = year_form.save()
            # person = person_form.save(commit=False)
            # person.year = year
            # person.save()
            # year, created = ClassYear.objects.get_or_create(Year=year_form.cleaned_data['Year'])
            # if not created:
            return HttpResponseRedirect('../')


        else:
            # ClassYear.objects.create(Year = y
            # )
            # Person.objects.create(
            #     Name = request.POST.get('Name'),
            #     Hobby = request.POST.get('Hobby'),
            #     Favorite = request.POST.get('Favorite'),
            #     Year = ClassYear.objects.create(
            #     Year = request.POST.get('Year')
            # )
            #     )
            messages.error(request, 'Year boi')

        # return HttpResponseRedirect('../')

            # # process the data in form.cleaned_data as required
            # # ...
            # # redirect to a new URL:
            # return HttpResponseRedirect('../')
    else:
        form = PersonForm()

    # else:
        # person_form = POSTPerson()
        # year_form = POSTClassYear()


        
    context = {
        'form':form
        # 'year_form': year_form,
        # 'person_form':person_form
    }


    return render(request, 'aboutme/addfriends.html', context)